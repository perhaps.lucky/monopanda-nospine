# MonoPanda

## Introduction

MonoPanda was created as personal project, to put some basic game engine ideas together. The most important part turned out to be the simple modability through json files and content files loaded from outside of pipeline.

MonoPanda is meant to to be used as "base engine project" rather than framework. There are many things that are not the strong part, and many things that could use some personal changes. In the end, what seems easy and simple to me, might seem like unnecessary complication to other person.

This version has removed Spine-related features (for users without Spine license). Documentation and Mod Manager can be downloaded from [main repository](https://gitlab.com/perhaps.lucky/monopanda).

## Used technologies

- [MonoGame](https://www.monogame.net/)
- [NoPipeline](https://github.com/Martenfur/NoPipeline) - configured to simply copy every file inside content directory (no need for compilation since everything gets loaded from raw files)
- [Penumbra](https://github.com/discosultan/penumbra) - 2D lighting component
- [ini-parser](https://github.com/rickyah/ini-parser)
- [Newtonsoft.Json](https://www.newtonsoft.com/json)
- [OptimizedPriorityQueue](https://github.com/BlueRaja/High-Speed-Priority-Queue-for-C-Sharp)
- [TextCopy](https://github.com/CopyText/TextCopy)


