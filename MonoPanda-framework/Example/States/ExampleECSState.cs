﻿using Consts;
using Example.ECS;
using Microsoft.Xna.Framework;
using MonoPanda;
using MonoPanda.BitmapFonts;
using MonoPanda.CollisionComponentDetails;
using MonoPanda.Components;
using MonoPanda.Console;
using MonoPanda.ECS;
using MonoPanda.GlobalTime;
using MonoPanda.Input;
using MonoPanda.Logger;
using MonoPanda.States;
using MonoPanda.Systems;

namespace Example.States {
  public class ExampleECSState : State {

    private EntityComponentSystem ecs;
    private Entity controlledEntity;

    private Entity leftBullet;
    private Entity rightBullet;

    private Entity solid;

    private Entity leftSolid;
    private Entity rightSolid;

    private Entity soundSource;

    public ExampleECSState(string id) : base(id) {
    }

    public override void Initialize() {
      ecs = EntityComponentSystem.LoadFromJson("ECS_Example/example-entity-component-system.json");

      ExampleFactory.CreateInstance<ExampleFactory>("ECS_Example/example-entity-factory.json", ecs);

      controlledEntity = ExampleFactory.CreateEntity("controlled-entity", new Vector2(0, 0)); 

      ExampleFactory.CreateEntity("gravity-source", new Vector2(500, -200));
      ExampleFactory.CreateEntity("gravity-source", new Vector2(500, 170));
      ExampleFactory.CreateEntity("gravity-source", new Vector2(-500, -200));

      leftBullet = ExampleFactory.CreateEntity("bullet", new Vector2(-2000, -200));
      rightBullet = ExampleFactory.CreateEntity("bullet", new Vector2(2000, -200));
      rightBullet.GetComponent<MoveComponent>().Velocity *= new Vector2(-1, 0);

      solid = ExampleFactory.CreateEntity("solid", new Vector2(0, 100));
      ExampleFactory.CreateEntity("solid", new Vector2(101, 100));

      leftSolid = ExampleFactory.CreateEntity("solid", new Vector2(-1000, 300));
      rightSolid = ExampleFactory.CreateEntity("solid", new Vector2(1000, 300));
      leftSolid.RemoveComponent<BehaviourComponent>();
      leftSolid.GetComponent<MoveComponent>().Velocity = new Vector2(100, 0);
      leftSolid.GetComponent<MoveComponent>().LockVelocity = true;
      rightSolid.RemoveComponent<BehaviourComponent>();
      rightSolid.GetComponent<MoveComponent>().Velocity = new Vector2(-100, 0);
      rightSolid.GetComponent<MoveComponent>().LockVelocity = true;

      ecs.GetSystem<CameraSystem>().FollowEntity(controlledEntity);
      //ecs.GetSystem<CameraSystem>().AddFollowedObject(new FollowedCursorObject(ecs.Camera, controlledEntity, 75));
      //ecs.GetSystem<CameraSystem>().AddFollowedObject(new FollowedEntityObject(solid));

      ecs.Camera.Position = new Vector2(-1000, 0);

      soundSource = ExampleFactory.CreateEntity("soundSource", new Vector2(-600, 600));

      ConsoleSystem.SetECS(ecs);
    }

    public override void Terminate() {

    }

    public override void Update() {
      //ecs.Camera.Position = controlledEntity.Position;
      ecs.Update();

      if (InputManager.GetKeyInput(Inputs.NextState).IsPressed)
        StateManager.SetActiveState(Consts.States.LoadingScreen, true);

      if (InputManager.GetKeyInput(Inputs.PreviousState).IsPressed)
        StateManager.SetActiveState(Consts.States.Timers, true);

      if (InputManager.GetKeyInput(Inputs.Exit).IsPressed)
        GameMain.ExitGame();

      if (InputManager.GetKeyInput(Inputs.Input4F).IsPressed) {
        Time.WorldTimeSpeed += -0.25f;
      }

      if (InputManager.GetKeyInput(Inputs.Input5G).IsPressed) {
        Time.WorldTimeSpeed += 0.25f;
      }

      if (InputManager.GetLeftMouseButton().IsHeld)
        controlledEntity.Rotation += 0.1f;

      if (InputManager.GetRightMouseButton().IsHeld)
        controlledEntity.Scale += 0.1f;

      if (InputManager.ScrolledUp())
        ecs.GetSystem<CameraSystem>().SmoothZoomIn(0.1f);

      if (InputManager.ScrolledDown())
        ecs.GetSystem<CameraSystem>().SmoothZoomOut(0.1f);

      if (InputManager.GetMiddleMouseButton().IsPressed) {
        soundSource.GetComponent<SoundEmitterComponent>().PlaySFX("example-sfx");
      }

      if (!leftBullet.IsDestroyed) {
        CollisionDetails details = leftBullet.GetComponent<CollisionComponent>().GetCollisionDetails(rightBullet);
        if (details.IsColliding) {
         /* leftBullet.Destroy();
          rightBullet.Destroy();*/
          leftBullet.GetComponent<MoveComponent>().Velocity = Vector2.Zero;
          rightBullet.GetComponent<MoveComponent>().Velocity = Vector2.Zero;
          var intersectionDistance = details.ThisCollisionRectangle.Right - details.OtherCollisionRectangle.Left;
          leftBullet.Position = details.ThisCollisionRectangle.Center - new Vector2(intersectionDistance / 2f, 0);
          rightBullet.Position = details.OtherCollisionRectangle.Center + new Vector2(intersectionDistance / 2f + 0.1f, 0);
        }
      }
    }

    public override void Draw() {
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 20), "ECS example", Color.GreenYellow, 40);
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 70), "Entity Component System example", Color.Yellow, 20);

      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 500), "M - next state (loading screen example)", Color.Gold, 20);
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 530), "N - previous state (timers example)", Color.Gold, 20);

      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 180), "A/D - move left/right", Color.AliceBlue, 20);
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 210), "Scroll up/down - zoom", Color.AliceBlue, 20);
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 240), "F/G - change world speed, current value: " + Time.WorldTimeSpeed, Color.AliceBlue, 20);

      var moveComponent = controlledEntity.GetComponent<MoveComponent>();
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(500, 470), "Position: " + moveComponent.Entity.Position, Color.Gold, 20);
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(500, 500), "Acceleration: " + moveComponent.Acceleration, Color.Gold, 20);
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(500, 530), "Velocity: " + moveComponent.Velocity, Color.Gold, 20);
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(500, 560), "Target zoom: " + ecs.GetSystem<CameraSystem>().SmoothZoomTarget + " Current zoom: " + ecs.Camera.Zoom, Color.Gold, 20);
      

      ecs.Draw();
    }
  }
}