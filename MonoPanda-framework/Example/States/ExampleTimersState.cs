﻿using Consts;
using Microsoft.Xna.Framework;
using MonoPanda;
using MonoPanda.BitmapFonts;
using MonoPanda.Input;
using MonoPanda.Logger;
using MonoPanda.Sound;
using MonoPanda.States;
using MonoPanda.Timers;

namespace Example.States {
  public class ExampleTimersState : State {

    private SingleTimeTimer singleTimeTimer;
    private PeriodTimer periodTimer;
    private RepeatingTimer repeatingTimer;

    public ExampleTimersState(string id) : base(id) { }

    public override void Initialize() {
      singleTimeTimer = new SingleTimeTimer(3000);
      periodTimer = new PeriodTimer(3000, 5000, 500);
      repeatingTimer = new RepeatingTimer(3000);
    }

    public override void Terminate() {
      singleTimeTimer.Dispose();
      periodTimer.Dispose();
      repeatingTimer.Dispose();
    }

    public override void Update() {
      if (InputManager.GetKeyInput(Inputs.Input1A).IsPressed) {
        singleTimeTimer.Initialize();
      }

      if (InputManager.GetKeyInput(Inputs.Input2S).IsPressed) {
        periodTimer.Initialize();
      }

      if (InputManager.GetKeyInput(Inputs.Input3D).IsPressed) {
        repeatingTimer.Initialize();
      }

      if (InputManager.GetKeyInput(Inputs.Input4F).IsPressed) {
        repeatingTimer.Dispose();
      }

      // terminating state to dispose timers - otherwise they will count their time in background
      if (InputManager.GetKeyInput(Inputs.NextState).IsPressed)
        StateManager.SetActiveState(Consts.States.ECS, true);

      if (InputManager.GetKeyInput(Inputs.PreviousState).IsPressed)
        StateManager.SetActiveState(Consts.States.Settings, true);

      if (InputManager.GetKeyInput(Inputs.Exit).IsPressed)
        GameMain.ExitGame();

      if (singleTimeTimer.Check()) {
        Log.log(LogCategory.Test, LogLevel.Debug, "Single time timer check passed!");
        SoundManager.PlaySFX(SFX.Example);
      }

      if (periodTimer.Check()) {
        Log.log(LogCategory.Test, LogLevel.Debug, "Period timer check passed!");
        SoundManager.PlaySFX(SFX.Example);
      }

      if (repeatingTimer.Check()) {
        Log.log(LogCategory.Test, LogLevel.Debug, "Repeating timer check passed!");
        SoundManager.PlaySFX(SFX.Example);
      }
    }

    public override void Draw() {
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 20), "Timers example", Color.GreenYellow, 40);
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 70), "Shows various timers. Timers will play sfx and log message when they pass.", Color.Yellow, 20);
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 150), "Actions: ", Color.AntiqueWhite, 20);
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 180), "A - initialize single time timer (3sec)", Color.AliceBlue, 20);
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 210), "S - initialize period timer (wait 3sec, then pass every 0.5sec for 5secs)", Color.AliceBlue, 20);
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 240), "D - initialize repeating timer (every 3sec)", Color.AliceBlue, 20);
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 270), "F - dispose repeating timer", Color.AliceBlue, 20);

      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 500), "M - next state (ECS example)", Color.Gold, 20);
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 530), "N - previous state (settings example)", Color.Gold, 20);

    }
  }
}