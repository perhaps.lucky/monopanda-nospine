﻿using Consts;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoPanda;
using MonoPanda.BitmapFonts;
using MonoPanda.Content;
using MonoPanda.GlobalTime;
using MonoPanda.Input;
using MonoPanda.States;
using MonoPanda.Utils;

namespace Example.States {
  public class ExampleContentState : State {
    private const int SQUARE_SPEED = 250;

    private Vector2 squarePosition;
    private bool goingRight;

    public ExampleContentState(string id) : base(id) { }

    public override void Initialize() {
      squarePosition = new Vector2(20, 300);
    }

    public override void Terminate() {

    }

    public override void Update() {
      if (InputManager.GetKeyInput(Inputs.Input1A).IsPressed)
        ContentLoader.PutRequest(new ContentRequest(ContentPackages.Example, true));

      // This is just for this example. I highly suggest to avoid reproducing it.
      if (InputManager.GetKeyInput(Inputs.Input2S).IsPressed)
        ContentLoader.loadPackage(ContentManager.getContentPackage(ContentPackages.Example));

      if (InputManager.GetKeyInput(Inputs.NextState).IsPressed)
        StateManager.SetActiveState(Consts.States.Language, false, true);

      if (InputManager.GetKeyInput(Inputs.PreviousState).IsPressed)
        StateManager.SetActiveState(Consts.States.Input);


      if (InputManager.GetKeyInput(Inputs.Exit).IsPressed)
        GameMain.ExitGame();

      if (goingRight) {
        squarePosition += new Vector2(SQUARE_SPEED, 0) * Time.ElapsedCalculated;
        if (squarePosition.X > 200)
          goingRight = false;
      } else {
        squarePosition += new Vector2(-SQUARE_SPEED, 0) * Time.ElapsedCalculated;
        if (squarePosition.X < 20)
          goingRight = true;
      }
    }

    public override void Draw() {
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 20), "Content example", Color.GreenYellow, 40);
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 70), "Shows content loading in the background thread vs normal loading \nContent will get unloaded after configured time. \nFor this example, content is drawn only when it's loaded and using content doesn't preserve it from unloading.", Color.Yellow, 20);
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 150), "Actions: ", Color.AntiqueWhite, 20);
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 180), "A - load content using request to ContentLoader", Color.AliceBlue, 20);
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 210), "S - load content in main thread", Color.AliceBlue, 20);

      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 500), "M - next state (language example)", Color.Gold, 20);
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 530), "N - previous state (inputs example)", Color.Gold, 20);

      // Dumb check but I need something that will return to false when it gets unloaded
      // Normally a Finished flag in ContentRequest object should be checked
      bool contentLoaded = ContentManager.getItem(Textures.BigFile).IsLoaded;
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 370), "Content loaded: " + contentLoaded, contentLoaded ? Color.LimeGreen : Color.Red);

      DrawUtils.DrawTexture(Textures.Square, squarePosition);
      drawIfLoaded(Textures.MonoPandaLogo, new Vector2(400, 150));
      drawIfLoaded(Textures.BigFile, new Vector2(700, 500));
    }

    // Method purely for this example.
    // Normally you should never ignore timer like this.
    private void drawIfLoaded(string textureId, Vector2 position) {
      ContentItem item = ContentManager.getItem(textureId);
      if (item.IsLoaded)
        GameMain.SpriteBatch.Draw(item.getItem<Texture2D>(ignoreTimer: true), position, Color.White);
    }
  }
}
