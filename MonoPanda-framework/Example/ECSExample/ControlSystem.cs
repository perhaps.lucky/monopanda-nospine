﻿using Consts;
using Microsoft.Xna.Framework;
using MonoPanda.Components;
using MonoPanda.ECS;
using MonoPanda.Input;
using MonoPanda.Utils;
using System;

namespace Example.ECS {
  public class ControlSystem : EntitySystem {

    public float AccelerationMultiplier { get; set; }

    public ControlSystem(string id, int updateInterval = -1) : base(id, updateInterval) {
    }

    public override void Update() {
      foreach (ControlComponent controlComponent in ECS.GetAllComponents<ControlComponent>()) {
        MoveComponent moveComponent = controlComponent.Entity.GetComponent<MoveComponent>();
        if (moveComponent != null) {
          updateMoveComponent(moveComponent, controlComponent);
        }

        LightComponent lightComponent = controlComponent.Entity.GetComponent<LightComponent>();
        if (lightComponent != null && controlComponent.RotateLightToCursor) {
          updateLightComponent(lightComponent);
        }
      }

    }

    private void updateMoveComponent(MoveComponent moveComponent, ControlComponent controlComponent) {
      moveComponent.Acceleration = new Vector2(); // always reset acceleration
      if (InputManager.GetKeyInput(Inputs.Input1A).IsHeld)
        moveComponent.Acceleration = new Vector2(-controlComponent.Acceleration, 0);

      if (InputManager.GetKeyInput(Inputs.Input3D).IsHeld)
        moveComponent.Acceleration = new Vector2(controlComponent.Acceleration, 0);

      if (InputManager.GetKeyInput(Inputs.Input6W).IsHeld)
        moveComponent.Acceleration = new Vector2(moveComponent.Acceleration.X, -controlComponent.Acceleration);

      if (InputManager.GetKeyInput(Inputs.Input2S).IsHeld)
        moveComponent.Acceleration = new Vector2(moveComponent.Acceleration.X, controlComponent.Acceleration);
    }

    private void updateLightComponent(LightComponent lightComponent) {
      var position = lightComponent.Entity.Position;
      var mouseInWorld = ECS.Camera.ScreenToWorld(InputManager.GetMouseWindowPosition());
      lightComponent.Rotation = RotationUtils.AimAt(position, mouseInWorld, Math.PI);
    }
  }
}
