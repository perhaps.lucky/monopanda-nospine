﻿using MonoPanda.ECS;

namespace Example.ECS {
  public class ControlComponent : EntityComponent {
    public float Acceleration { get; set; }
    public bool RotateLightToCursor { get; set; }
  }
}
