﻿using IniParser.Model;
using MonoPanda.UtilityClasses;

namespace MonoPanda.Configuration {
  public class TimeConfig : DefaultIniSectionLoader {

    public readonly float ElapsedCalculatedMultiplier;

    public TimeConfig(IniData iniData) : base(iniData, "Time") { }
  }
}
