﻿using IniParser.Model;
using MonoPanda.UtilityClasses;

namespace MonoPanda.Configuration {
  public class MouseConfig : DefaultIniSectionLoader {

    public readonly bool CursorVisibleAtStart;
    public readonly bool DefaultCursor;
    public readonly string CursorSpriteContentId;

    public MouseConfig(IniData iniData) : base(iniData, "Mouse") { }
  }
}
