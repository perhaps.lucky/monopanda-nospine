﻿using MonoPanda.GlobalTime;
using MonoPanda.Logger;
using System;

namespace MonoPanda.Timers {
  public abstract class Timer : IDisposable {

    private static int currentId = 0;

    public string Id { get; private set; }

    protected int workTime;
    protected float elapsedTime;

    protected bool affectedByWorldTime;

    protected LogCategory logCategory;

    public Timer(int expectedWorkTime, string timerType, string id = null, bool affectedByWorldTime = false, LogCategory logCategory = LogCategory.Timers) {
      this.workTime = expectedWorkTime;
      this.Id = (id==null ? (currentId++).ToString() : id) + "_" + timerType;
      this.logCategory = logCategory;
      this.affectedByWorldTime = affectedByWorldTime;
    }

    /// <summary>
    /// (Re-)Starts timer.
    /// </summary>
    /// <param name="silent">if true then it won't get logged (used in some system timers)</param>
    public virtual void Initialize(bool silent = false) {
      if (!silent)
        Log.log(logCategory, LogLevel.Info, "Timer id: " + Id + " initialized.");
      elapsedTime = 0;
      TimersSystem.unregisterTimer(this, silent);
      TimersSystem.registerTimer(this, silent);
    }

    public void update() {
      Log.log(logCategory, LogLevel.Debug, "Updating timer id: " + Id);
      elapsedTime += Time.ElapsedMillis * (affectedByWorldTime ? Time.WorldTimeSpeed : 1.0f);
      if (elapsedTime >= workTime)
        TimersSystem.unregisterTimer(this);

      Log.log(logCategory, LogLevel.Debug, "Timer id: " + Id + " status: " + elapsedTime + " / " + workTime);
    }

    public abstract bool Check();

    /// <summary>
    /// Stops timer and unregisters it from system.
    /// </summary>
    public void Dispose() {
      TimersSystem.unregisterTimer(this, true);
    }

    public float GetProgress() {
      return (float)elapsedTime / workTime;
    }
  }
}
