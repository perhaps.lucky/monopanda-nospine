﻿using MonoPanda.Logger;

namespace MonoPanda.Timers {
  /// <summary>
  /// This timer check returns true every time repeatTime has passed.
  /// </summary>
  public class RepeatingTimer : Timer {

    public RepeatingTimer(int repeatTime, string id = null, bool affectedByWorldTime = false, LogCategory logCategory = LogCategory.Timers) : base(repeatTime, "RepeatingTimer", id, affectedByWorldTime, logCategory) {
    }

    /// <summary>
    /// Returns true every time repeatTime has passed
    /// </summary>
    public bool Check(bool silent = true) {
      if (elapsedTime >= workTime) {
        Initialize(silent);
        return true;
      }
      return false;
    }

    public override bool Check() {
      return Check();
    }
  }
}
