﻿using MonoPanda.Logger;
using System.Collections.Generic;

namespace MonoPanda.Timers {
  public class TimersSystem {

    private static List<Timer> registeredTimers = new List<Timer>();

    private static List<Timer> timersToRegister = new List<Timer>();
    private static List<Timer> timersToUnregister = new List<Timer>();

    public static void registerTimer(Timer timer, bool silent = false) {
      if (!silent)
        Log.log(LogCategory.TimersSystem, LogLevel.Debug, "Timer id: " + timer.Id + " queued for register.");

      lock (timersToRegister) {
        if (!timersToRegister.Contains(timer))
          timersToRegister.Add(timer);
      }
    }

    public static void unregisterTimer(Timer timer, bool silent = false) {
      if (!silent)
        Log.log(LogCategory.TimersSystem, LogLevel.Debug, "Timer id: " + timer.Id + " queued for unregister.");
      lock (timersToUnregister) {
        if (!timersToUnregister.Contains(timer))
          timersToUnregister.Add(timer);
      }
    }

    public static void update() {
      Log.log(LogCategory.TimersSystem, LogLevel.Info, "Updating registered timers.");
      foreach (Timer timer in registeredTimers) {
        timer.update();
      }

      lock (timersToRegister)
        lock (timersToUnregister) {
          Log.log(LogCategory.TimersSystem, LogLevel.Info, "Unregistering queued timers.");
          registeredTimers.RemoveAll(t => timersToUnregister.Contains(t));
          Log.log(LogCategory.TimersSystem, LogLevel.Info, "Registering queued timers.");
          registeredTimers.AddRange(timersToRegister);
          timersToRegister.Clear();
          timersToUnregister.Clear();
        }
    }

  }
}
