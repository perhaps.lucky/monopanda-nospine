﻿using MonoPanda.Logger;

namespace MonoPanda.Timers {
  /// <summary>
  /// This timer check returns true once after the waitTime has passed.
  /// Afterwards it needs another initialization to run.
  /// </summary>
  public class SingleTimeTimer : Timer {

    private bool gotChecked;

    public SingleTimeTimer(int waitTime, string id = null, bool affectedByWorldTime = false, LogCategory logCategory = LogCategory.Timers) : base(waitTime, "SingleTimeTimer", id, affectedByWorldTime, logCategory) {
    }

    public override void Initialize(bool silent = false) {
      base.Initialize(silent);
      gotChecked = false;
    }

    /// <summary>
    /// Returns true once when the waitTime has passed.
    /// </summary>
    public override bool Check() {
      if (!gotChecked && this.elapsedTime >= this.workTime) {
        gotChecked = true;
        return true;
      }
      return false;
    }
  }
}
