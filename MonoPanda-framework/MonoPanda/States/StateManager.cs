﻿using MonoPanda.Configuration;
using MonoPanda.Logger;
using MonoPanda.Utils;
using System;
using System.Collections.Generic;

namespace MonoPanda.States {
  public class StateManager {
    private static StateManager instance;
    private State activeState;
    private List<State> states;

    private StateManager() { }

    public static void initialize() {
      var instance = getInstance();
      instance.states = new List<State>();
      Dictionary<string, string> dicc = ContentUtils.LoadJson<Dictionary<string, string>>(Config.States.StatesJsonPath);

      foreach (KeyValuePair<string, string> keyValuePair in dicc) {
        Type type = Type.GetType(keyValuePair.Value);
        State state = (State)Activator.CreateInstance(type, keyValuePair.Key);
        instance.states.Add(state);
      }

      SetActiveState(Config.States.InitialStateId);
    }

    public static void update() {
      getInstance().activeState.Update();
    }

    public static void draw() {
      getInstance().activeState.Draw();
    }

    public static void SetActiveState(string id, bool terminateCurrentState = false, bool reinitialize=false) {
      if (terminateCurrentState) {
        Log.log(LogCategory.States, LogLevel.Info, "Terminating state id: " + getInstance().activeState.Id);
        getInstance().activeState.Terminate();
        getInstance().activeState.Initialized = false;
      }

      State state = getState(id);
      Log.log(LogCategory.States, LogLevel.Info, "Setting active state to: " + id);
      getInstance().activeState = state;
      if (!state.Initialized) {
        Log.log(LogCategory.States, LogLevel.Info, "Initializing state id: " + id);
        state.Initialize();
        state.Initialized = true;
      } else if (reinitialize) {
        Log.log(LogCategory.States, LogLevel.Info, "Reinitializing state id: " + id);
        state.Terminate();
        state.Initialize();
      }
    }

    private static State getState(string id) {
      return getInstance().states.Find(s => id.Equals(s.Id));
    }

    public static StateManager getInstance() {
      if (instance == null)
        instance = new StateManager();
      return instance;
    }

    
  }
}
