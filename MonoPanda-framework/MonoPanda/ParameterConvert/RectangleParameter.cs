﻿using Microsoft.Xna.Framework;

using MonoPanda.Utils;

namespace MonoPanda.ParameterConvert {
  public sealed class RectangleParameter : ParameterConvertAttribute {

    private Vector2 offsetTo;
    private bool offset;

    public RectangleParameter(Vector2 offsetTo = default, bool offset = false) {
      this.offsetTo = offsetTo;
      this.offset = offset;
    }

    public override object Convert(string parameterValue) {
      return ParameterUtils.GetRectangle(parameterValue, offsetTo, offset);
    }
  }
}
