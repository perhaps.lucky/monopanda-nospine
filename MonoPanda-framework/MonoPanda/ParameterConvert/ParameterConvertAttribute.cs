﻿using System;

namespace MonoPanda.ParameterConvert {
  public abstract class ParameterConvertAttribute : Attribute {
    public abstract object Convert(string parameterValue);
  }
}
