﻿using Microsoft.Xna.Framework;

using MonoPanda.Utils;

namespace MonoPanda.ParameterConvert {
  public sealed class ColorParameter : ParameterConvertAttribute {
    public override object Convert(string parameterValue) {
      if (parameterValue.Contains(",")) {
        return ParameterUtils.GetColor(parameterValue);
      } else {
        return ReflectionUtils.GetStaticProperty<Color>(typeof(Color), parameterValue);
      }
    }
  }
}
