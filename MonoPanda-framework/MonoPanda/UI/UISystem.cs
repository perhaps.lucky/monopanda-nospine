﻿using System.Collections.Generic;

namespace MonoPanda.UI {
  public class UISystem {

    private List<UIObject> objects;
    private bool clearCalled;

    public UISystem() {
      objects = new List<UIObject>();
    }

    public void Update() {
      clearCalled = false;
      foreach (var o in objects) {
        o.UpdateIfVisible();
        if (clearCalled)
          return;
      }
    }

    public void Draw() {
      foreach (var o in objects) {
        o.DrawIfVisible();
      }
    }

    public void AddObject(UIObject UIObject) {
      objects.Add(UIObject);
    }

    public void Clear() {
      objects.Clear();
      clearCalled = true;
    }

    public void ForceOnChangeActions() {
      foreach (var o in objects) {
        if (o is IForceOnChange)
          (o as IForceOnChange).ForceOnChange();
      }
    }


  }
}
