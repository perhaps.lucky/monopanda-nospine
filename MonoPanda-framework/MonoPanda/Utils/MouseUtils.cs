﻿using Microsoft.Xna.Framework;
using MonoPanda.Input;

namespace MonoPanda.Utils {
  public class MouseUtils {
    public static bool IsCursorInRectangleInWindow(Vector2 leftTopCorner, Vector2 rightDownCorner) {
      var mousePos = InputManager.GetMouseWindowPosition();
      var rect = new Rectangle((int)leftTopCorner.X, (int)leftTopCorner.Y, (int)(rightDownCorner.X - leftTopCorner.X), (int)(rightDownCorner.Y - leftTopCorner.Y));

      return rect.Contains(mousePos);
    }
  }
}
