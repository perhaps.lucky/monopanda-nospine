﻿using Microsoft.Xna.Framework;
using System;

namespace MonoPanda.Utils {
  public class RotationUtils {
    public static Vector2 RotateWithAnchor(Vector2 vector, float rotation, Vector2 anchor) {
      vector -= anchor;
      vector = Vector2.Transform(vector, Matrix.CreateRotationZ(rotation));
      vector += anchor;
      return vector;
    }

    /// <summary>
    /// Counts Rotation value to aim at given position
    /// </summary>
    /// <param name="basePosition">position of item that rotation is counted for</param>
    /// <param name="aimPosition">position to aim at</param>
    /// <param name="correction">correction of rotation, usually divided Math.PI value</param>
    /// <returns></returns>
    public static float AimAt(Vector2 basePosition, Vector2 aimPosition, double correction = (float)Math.PI / 2.0f) {
      var angle = Math.Atan2(basePosition.Y - aimPosition.Y, basePosition.X - aimPosition.X);
      return (float)angle - (float)correction;
    }
  }
}
