﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoPanda.Content;
using MonoPanda.Systems;
using MonoPanda.ECS;
using MonoPanda.ECS.Tools;
using MonoPanda.ECS.Components.CollisionComponentDetails;

namespace MonoPanda.Utils {
  public class DrawUtils {

    public static void DrawTexture(string id, Vector2 position, SpriteBatch spriteBatch = null) {
      DrawTexture(id, position, Color.White, spriteBatch);
    }

    public static void DrawTexture(string id, Vector2 position, Color color, SpriteBatch spriteBatch = null) {
      Texture2D texture = ContentManager.Get<Texture2D>(id);
      if (texture != null) {
        if (spriteBatch != null)
          spriteBatch.Draw(texture, position, color);
        else
          GameMain.SpriteBatch.Draw(texture, position, color);
      }
    }

    /// <summary>
    /// Request a draw of rectangle. <br/>
    /// <b>Use only for dev purposes.</b>
    /// </summary>
    public static void DrawRectangle(EntityComponentSystem ecs, Rectangle rectangle, Color color, float depth = 0) {
      DrawSystem drawSystem = ecs.GetSystem<DrawSystem>();
      if (drawSystem == null) {
        return;
      }

      drawSystem.Rectangles.Add(new DrawRectangle(rectangle, color, depth));
    }

    public static void DrawRectangle(EntityComponentSystem ecs, CollisionRectangle rectangle, Color color, float depth = 0) {
      DrawSystem drawSystem = ecs.GetSystem<DrawSystem>();
      if (drawSystem == null) {
        return;
      }

      drawSystem.Rectangles.Add(new DrawRectangle(rectangle, color, depth));
    }
  }
}
