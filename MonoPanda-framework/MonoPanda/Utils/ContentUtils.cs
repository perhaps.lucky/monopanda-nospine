﻿using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using MonoPanda.Configuration;
using MonoPanda.Multithreading;
using Newtonsoft.Json;
using System.IO;
using System.Xml.Serialization;

namespace MonoPanda.Utils {
  public class ContentUtils {

    public static Texture2D LoadTexture(StreamHolder holder) {
      using (holder) {
        Texture2D texture2D = Texture2D.FromStream(GameMain.GraphicsDeviceManager.GraphicsDevice, holder.Open());
        return texture2D;
      }
    }


    public static Texture2D LoadTexture(string path) {
      using (FileStream fileStream = new FileStream(path, FileMode.Open)) {
        Texture2D texture2D = Texture2D.FromStream(GameMain.GraphicsDeviceManager.GraphicsDevice, fileStream);
        return texture2D;
      }
    }

    public static SoundEffect LoadSoundEffect(string path) {
      using (FileStream fileStream = new FileStream(path, FileMode.Open)) {
        SoundEffect soundEffect = SoundEffect.FromStream(fileStream);
        return soundEffect;
      }
    }

    public static T LoadXml<T>(string path) {
      XmlSerializer deserializer = new XmlSerializer(typeof(T));
      T target;
      using (TextReader textReader = new StreamReader(path))
        target = (T)deserializer.Deserialize(textReader);
      return target;
    }

    public static T LoadJson<T>(string path) {
      return JsonConvert.DeserializeObject<T>(File.ReadAllText(path));
    }
  }
}
