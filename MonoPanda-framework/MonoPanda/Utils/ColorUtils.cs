﻿using Microsoft.Xna.Framework;

namespace MonoPanda.Utils {
  public class ColorUtils {

    public static Color MixColors(Color first, Color second) {
      return new Color(
        (first.R + second.R) / 2,
        (first.G + second.G) / 2,
        (first.B + second.B) / 2,
        (first.A + second.A) / 2);
    }

    public static float ByteToFloat(byte value) {
      return value / 255.0f;
    }

    public static byte FloatToByte(float value) {
      return (byte)(value * 255);
    }
  }
}
