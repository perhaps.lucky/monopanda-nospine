﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MonoPanda.Utils {
  public class TextureUtils {

    public static Texture2D ChangeColorToTransparent(Texture2D texture, Color changedColor, bool disposeOldTexture = true) {
      Color[] colorData = getColorDataFromTexture(texture);

      if (disposeOldTexture)
        texture.Dispose();

      for (int i = 0; i < texture.Width * texture.Height; i++) {
        if (colorData[i].R == changedColor.R
          && colorData[i].G == changedColor.G
          && colorData[i].B == changedColor.B) {
          colorData[i].A = 0;
        }
      }

      return createTextureFromColorData(colorData, texture.Width, texture.Height);
    }

    public static Texture2D ChangeColorsForBitmapFont(Texture2D texture) {
      Color[] colorData = getColorDataFromTexture(texture);
      texture.Dispose(); // always dispose in this case

      for (int i = 0; i < texture.Width * texture.Height; i++) {
        // assuming all fonts are on black background with white text
        colorData[i].A = colorData[i].R;
      }
      return createTextureFromColorData(colorData, texture.Width, texture.Height);
    }

    public static Texture2D CreateRectangleTexture(int width, int height, Color color) {
      var texture = new Texture2D(GameMain.GraphicsDeviceManager.GraphicsDevice, width, height);
      Color[] textureData = new Color[width * height];
      for (int i = 0; i < textureData.Length; i++) {
        textureData[i] = color;
      }
      texture.SetData(textureData);
      return texture;
    }

    private static Color[] getColorDataFromTexture(Texture2D texture) {
      Color[] data = new Color[texture.Width * texture.Height];
      texture.GetData(data);
      return data;
    }

    private static Texture2D createTextureFromColorData(Color[] data, int width, int height) {
      Texture2D texture = new Texture2D(GameMain.GraphicsDeviceManager.GraphicsDevice, width, height);
      texture.SetData(data);
      return texture;
    }

  }
}
