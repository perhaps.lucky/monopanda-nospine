﻿using MonoPanda.Configuration;
using MonoPanda.Mods;

using System;
using System.IO;
using System.Linq;

namespace MonoPanda.Utils {
  public class ModsUtils {

    public static void ForEachModFile(string relativePath, Action<string> doAction, bool includeCoreMod = true) {
      foreach (string modName in ModManager.GetActiveMods(includeCoreMod).Select(m => m.Name)) {
        string fullPath = GetModPath(modName, relativePath);
        if (File.Exists(fullPath))
          doAction.Invoke(fullPath);
      }

    }

    public static void ForEachModName(Action<string> action, bool includeCoreMod = true) {
      foreach (string modName in ModManager.GetActiveMods(includeCoreMod).Select(m => m.Name)) {
        action.Invoke(modName);
      }
    }

    public static void ForEachMod(Action<ModSettings> action, bool includeCoreMod = true) {
      foreach (ModSettings mod in ModManager.GetActiveMods(includeCoreMod)) {
        action.Invoke(mod);
      }
    }


    public static string GetModPath(string modName, string pathToFile) {
      return Config.Mods.ModsFolder + modName + "/" + pathToFile;
    }

  }
}
