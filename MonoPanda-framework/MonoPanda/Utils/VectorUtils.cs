﻿using Microsoft.Xna.Framework;

namespace MonoPanda.Utils {
  public class VectorUtils {

    public static Vector2 GetDirectionVector(Vector2 from, Vector2 to) {
      return Vector2.Normalize(to - from);
    }

    public static Vector2 Integrals(Vector2 source) {
      return new Vector2((int)source.X, (int)source.Y);
    }

    public static Vector2 Vector3ToVector2(Vector3 source) {
      return new Vector2(source.X, source.Y);
    }


  }
}
