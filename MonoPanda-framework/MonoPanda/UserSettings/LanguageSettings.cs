﻿using IniParser.Model;
using MonoPanda.UtilityClasses;

namespace MonoPanda.UserSettings {
  public class LanguageSettings : SettingsIniSectionLoader {

    public string Language { get; set; }

    public LanguageSettings(IniData iniData) : base(iniData, "Language") { }
  }
}
