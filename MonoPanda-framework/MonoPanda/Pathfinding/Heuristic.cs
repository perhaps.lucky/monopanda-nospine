﻿namespace MonoPanda.Pathfinding {
  public abstract class Heuristic {

    public abstract int count(Node node, SearchInstance searchInstance);
  }
}
