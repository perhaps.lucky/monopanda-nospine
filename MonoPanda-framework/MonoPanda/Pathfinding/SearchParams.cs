﻿using System.Collections.Generic;

namespace MonoPanda.Pathfinding {
  public class SearchParams {
    public List<int> OpenNodes { get; private set; }
    public List<int> ClosedNodes { get; private set; }
    public Dictionary<int, int> TravelCost { get; private set; }
    public Dictionary<int, int> Heuristic { get; private set; }
    public Dictionary<int, int> TotalCost { get; private set; }
    public Dictionary<int, int> Parent { get; private set; }
    public int StartNode { get; private set; }
    public int TargetNode { get; private set; }


    public SearchParams(int startNode, int targetNode) {
      StartNode = startNode;
      TargetNode = targetNode;
      OpenNodes = new List<int>();
      ClosedNodes = new List<int>();
      TravelCost = new Dictionary<int, int>();
      Heuristic = new Dictionary<int, int>();
      TotalCost = new Dictionary<int, int>();
      Parent = new Dictionary<int, int>();
    }
  }
}
