﻿using MonoPanda.Multithreading;
using System.Collections.Generic;

namespace MonoPanda.Pathfinding {
  public class PathfindingRequest : ThreadRequest {
    public Node Start { get; set; }
    public Node Target { get; set; }
    public bool FoundPath { get; set; }
    public List<Node> Path { get; set; } = new List<Node>();
  }
}
