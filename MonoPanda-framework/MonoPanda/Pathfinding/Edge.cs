﻿using System;

namespace MonoPanda.Pathfinding {
  public class Edge {
    public int TravelCost { get; set; }

    // edges work both way so these names are just names
    public Node From { get; private set; }
    public Node To { get; private set; }

    public Edge(Node from, Node to, int cost) {
      From = from;
      To = to;
      TravelCost = cost;
    }

    public Node GetOtherEnd(Node thisNode) {
      if (thisNode == From)
        return To;
      if (thisNode == To)
        return From;

      throw new Exception("thisNode doesn't belong to this edge");
    }

    public int GetOtherEndID(int thisNodeID) {
      if (thisNodeID == From.ID)
        return To.ID;
      if (thisNodeID == To.ID)
        return From.ID;

      throw new Exception("thisNodeID doesn't belong to this edge");
    }
  }
}
