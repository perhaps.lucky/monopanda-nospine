﻿using Microsoft.Xna.Framework;
using System;

namespace MonoPanda.Pathfinding {
  public class DistanceHeuristic : Heuristic {

    public Func<Node, Point> positionProvider;

    public DistanceHeuristic(Func<Node, Point> positionProvider) {
      this.positionProvider = positionProvider;
    }

    public override int count(Node node, SearchInstance searchInstance) {
      var nodePosition = positionProvider.Invoke(node);
      var targetPosition = positionProvider.Invoke(searchInstance.PathfindingSystem.GetNode(searchInstance.SearchParams.TargetNode));
      return Math.Abs(nodePosition.X - targetPosition.X) + Math.Abs(nodePosition.Y - targetPosition.Y);
    }

  }
}
