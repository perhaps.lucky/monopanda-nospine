﻿namespace MonoPanda.Mods {
  public class ModInfo {
    public string FullName { get; set; }
    public string Description { get; set; }
    public string Author { get; set; }
    public string Mail { get; set; }
    public string Website { get; set; }
    public string Image { get; set; }

    public override string ToString() {
      return 
        "Mod info:" +
        (FullName != null ? "\nName: " + FullName : "") +
        (Author != null ? "\nAuthor: " + Author : "") +
        (Mail != null ? "\nMail: " + Mail : "") +
        (Website != null ? "\nWebsite: " + Website : "") +
        (Description != null ? "\nDescription:\n" + Description : "");
    }
  }
}
