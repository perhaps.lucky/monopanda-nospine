﻿using MonoPanda.Configuration;
using MonoPanda.Logger;


namespace MonoPanda.Initializers {
  public class MouseCursorInitializer {

    public static void initialize() {
      Log.log(LogCategory.Startup, LogLevel.Info, "Loading mouse cursor settings.");
      GameSettings.getInstance().DrawCustomCursor = !Config.Mouse.DefaultCursor;
      GameSettings.getInstance().IsCursorVisible = Config.Mouse.CursorVisibleAtStart;
      GameSettings.getInstance().CustomCursorContentId = Config.Mouse.CursorSpriteContentId;

      GameMain.GetInstance().IsMouseVisible = GameSettings.getInstance().IsCursorVisible ? !GameSettings.getInstance().DrawCustomCursor : false;
    }
  }
}
