﻿using MonoPanda.Configuration;
using MonoPanda.Logger;
using MonoPanda.UserSettings;

namespace MonoPanda.Initializers {
  public class GraphicsInitializer {

    public static void initialize() {
      if (Config.Window.AllowUserSettings)
        loadUserSettings();
      else
        loadConfig();

      GameMain.GraphicsDeviceManager.ApplyChanges();
    }

    public static void loadUserSettings() {
      Log.log(LogCategory.Startup, LogLevel.Info, "Loading user graphics settings.");
      GameMain.GraphicsDeviceManager.PreferredBackBufferWidth = Settings.Window.Width;
      GameMain.GraphicsDeviceManager.PreferredBackBufferHeight = Settings.Window.Height;
      GameMain.GraphicsDeviceManager.IsFullScreen = Settings.Window.Fullscreen;
    }

    public static void loadConfig() {
      Log.log(LogCategory.Startup, LogLevel.Info, "Loading engine graphics settings.");
      GameMain.GraphicsDeviceManager.PreferredBackBufferWidth = Config.Window.Width;
      GameMain.GraphicsDeviceManager.PreferredBackBufferHeight = Config.Window.Height;
      GameMain.GraphicsDeviceManager.IsFullScreen = Config.Window.Fullscreen;
    }

  }
}
