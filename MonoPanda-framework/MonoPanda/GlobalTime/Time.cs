﻿using Microsoft.Xna.Framework;

using MonoPanda.Components;
using MonoPanda.Configuration;
using MonoPanda.ECS;

namespace MonoPanda.GlobalTime {
  public class Time {

    public static GameTime CurrentGameTime { get; private set; }
    public static float ElapsedMillis => (float)CurrentGameTime.ElapsedGameTime.TotalMilliseconds;
    public static float ElapsedCalculated => (float)calculateElapsed();

    /// <summary>
    /// This can be used whenever there is a time-affecting feature. Timers and systems can be affected by this value.
    /// </summary>
    public static float WorldTimeSpeed { get; set; } = 1.0f;

    /// <summary>
    /// This can be used like WorldTimeSpeed, with the difference that entity with TimeComponent have another way for individual time speed control
    /// </summary>
    public static float EntityTimeSpeed(Entity entity) {
      var entityMultiplier = 1.0f;
      entity.GetComponentOptional<TimeComponent>().IfPresent(c => entityMultiplier = c.TimeMultiplier);
      return WorldTimeSpeed * entityMultiplier;
    }

    public static float EntityTimeSpeed(EntityComponent component) {
      return EntityTimeSpeed(component.Entity);
    }

    public static void update(GameTime gameTime) {
      CurrentGameTime = gameTime;
    }

    private static double calculateElapsed() {
      return CurrentGameTime.ElapsedGameTime.TotalMilliseconds * Config.Time.ElapsedCalculatedMultiplier;
    }

  }
}
