﻿using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace MonoPanda.ECS {
  public class Entity {

    public string Id { get; private set; }
    public Vector2 Position { get => position; set { position = value; informObservers(); } }
    public float Scale { get => scale; set { scale = value; informObservers(); } }
    public float Rotation { get => rotation; set { rotation = value; informObservers(); } }
    public bool IsInVisibleArea { get; set; }
    public bool IsDestroyed { get; private set; }

    private Vector2 position;
    private float scale = 1f;
    private float rotation;
    
    public EntityComponentSystem EntityComponentSystem { get; private set; }
    private List<EntityComponent> components;
    private List<EntityObserver> observers;

    public Entity(string id, Vector2 position, EntityComponentSystem ecs) {
      observers = new List<EntityObserver>();
      Id = id;
      Position = position;
      EntityComponentSystem = ecs;
      components = new List<EntityComponent>();
    }

    public void Destroy() {
      foreach (EntityComponent component in components) {
        component.Destroy();
        EntityComponentSystem.ComponentRemoved(component);
      }
      EntityComponentSystem.RemoveEntity(this);
      IsDestroyed = true;
    }

    public void AddComponent(EntityComponent component) {
      components.Add(component);
      component.Entity = this;
      component.TypeIdentifier = component.GetType().FullName;
      component.ShortIdentifier = component.GetType().Name;
      EntityComponentSystem.ComponentAdded(component);
    }

    public void RemoveComponent<T>() where T : EntityComponent {
      var component = GetComponent<T>();
      if (component != null) {
        RemoveComponent(component);
      }
      EntityComponentSystem.ComponentRemoved(component);
    }

    public void RemoveComponent(EntityComponent component) {
      component.Destroy();
      components.Remove(component);
    }

    public bool HasComponent<T>() where T : EntityComponent {
      return GetComponent<T>() != null;
    }

    public T GetComponent<T>() where T : EntityComponent {
      return components.Find(c => c is T) as T;
    }

    public EntityComponent GetComponentByIdentifier(string shortIdentifier) {
      return components.Find(c => c.ShortIdentifier.Equals(shortIdentifier));
    }

    public Optional<T> GetComponentOptional<T>() where T : EntityComponent {
      return Optional<T>.Of(components.Find(c => c is T) as T);
    }

    public List<T> GetAllComponents<T>() where T : EntityComponent {
      return components.FindAll(c => c is T).ConvertAll(c => c as T);
    }

    public T GetComponentById<T>(string id) where T : EntityComponent {
      return components.Find(c => c is T && c.Id.Equals(id)) as T;
    }

    public EntityObserver RegisterObserver(EntityObserver observer) {
      observers.Add(observer);
      return observer;
    }

    public void UnregisterObserver(EntityObserver observer) {
      observers.Remove(observer);
    }

    private void informObservers() {
      foreach (EntityObserver observer in observers) {
        observer.Inform();
      }
    }

    public override string ToString() {
      var str = "%{Cyan}% " + Id + "\n"
        + "%{PaleGoldenrod}% Position: %{White}% " + Position.ToString() + "\n"
        + "%{PaleGoldenrod}% Scale: %{White}% " + Scale.ToString() + "\n"
        + "%{PaleGoldenrod}% Rotation: %{White}% " + Scale.ToString() + "\n"
        + "%{PaleGoldenrod}% Visible: " + (IsInVisibleArea ? "%{Lime}% Yes" : "%{OrangeRed}% No") + "\n";

      foreach (var component in components) {
        str += component.ToString() + "\n";
      }

      return str.Substring(0, str.Length-1);
    }
  }
}
