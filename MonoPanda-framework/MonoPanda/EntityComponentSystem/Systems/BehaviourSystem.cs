﻿using MonoPanda.Components;
using MonoPanda.ECS;

namespace MonoPanda.Systems {
  public class BehaviourSystem : EntitySystem {
    public BehaviourSystem(string id, int updateInterval = -1) : base(id, updateInterval) {
    }

    public override void Update() {
      foreach (BehaviourComponent component in ECS.GetAllComponents<BehaviourComponent>()) {
        component.Behaviour.Update();
      }
    }

    public override string ToString() {
      return "%{LemonChiffon}% BehaviourSystem";
    }
  }
}
