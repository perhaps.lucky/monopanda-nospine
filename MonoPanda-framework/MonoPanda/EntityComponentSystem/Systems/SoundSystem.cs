﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

using MonoPanda.Components;
using MonoPanda.ECS;
using MonoPanda.Sound;
using MonoPanda.Utils;

using System.Collections.Generic;

namespace MonoPanda.Systems {
  public class SoundSystem : EntitySystem {

    // Below this distance value, 3D effects play at max volume
    public float DefaultDistanceOfMaxVolume { get; set; } = 50f;
    // Below this distance value(but above DefaultDistanceOfMaxVolume), 3D effects play at scaled volume (using virtual distance)
    // Above this value, 3D effects are unhearable
    public float DefaultDistanceOfMinVolume { get; set; } = 1000f;
    // Multiplier used to count "virtual distance" between emitter and listener
    public float DistanceScaleMultiplier { get; set; } = 20f;

    public AudioListener ActiveAudioListener { get; set; }

    public SoundSystem(string id, int updateInterval = -1) : base(id, updateInterval) {
    }

    public override void Update() {
      lookForListener();
      updateListeners();
      updateEmitters();
    }

    public SoundEffectInstance PlaySFX(string id, AudioEmitter audioEmitter, float pitch = 0f) {
      if (ActiveAudioListener != null) {
        var sfxInstance = PlaySFX(id);
        sfxInstance.Apply3D(ActiveAudioListener, audioEmitter);
        sfxInstance.Pitch = pitch;
        return sfxInstance;
      }
      return null;
    }

    public SoundEffectInstance PlaySFX(string id, Vector3 position, float pitch = 0f) {
      var tempEmitter = new AudioEmitter();
      tempEmitter.Position = position;
      return PlaySFX(id, tempEmitter, pitch);
    }

    public SoundEffectInstance PlaySFX(string id, Vector2 position, float pitch = 0f) {
      return PlaySFX(id, new Vector3(position, 0), pitch);
    }

    // Next methods are proxing SoundManager methods and they are here just for convenience
    public SoundEffectInstance PlaySFX(string id) {
      return SoundManager.PlaySFX(id);
    }

    public void PlayMusic(string id, bool loop = true) {
      SoundManager.PlayMusic(id, loop);
    }

    public void StopMusic() {
      SoundManager.StopMusic();
    }

    public string GetCurrentlyPlayedSongId() {
      return SoundManager.GetCurrentlyPlayedSongId();
    }

    public void SetSFXVolume(int volume) {
      SoundManager.SetSFXVolume(volume);
    }

    public void SetMusicVolume(int volume) {
      SoundManager.SetMusicVolume(volume);
    }

    public void AdjustSFXVolume(int change) {
      SoundManager.AdjustSFXVolume(change);
    }

    public void AdjustMusicVolume(int change) {
      SoundManager.AdjustMusicVolume(change);
    }
    private void lookForListener() {
      if (ActiveAudioListener == null) {
        List<SoundListenerComponent> listeners = ECS.GetAllComponents<SoundListenerComponent>();
        if (listeners.Count > 0)
          ActiveAudioListener = listeners[0].AudioListener; // in most scenerios there will be just 1 possible listener, so set him as default
      }
    }

    private void updateEmitters() {
      foreach (var soundEmitter in ECS.GetAllComponents<SoundEmitterComponent>()) {
        var realDistance = Vector2.Distance(soundEmitter.Entity.Position, VectorUtils.Vector3ToVector2(ActiveAudioListener.Position));
        var distanceOfMaxVolume = soundEmitter.DistanceOfMaxVolume != -1f ? soundEmitter.DistanceOfMaxVolume : DefaultDistanceOfMaxVolume;
        var distanceOfMinVolume = soundEmitter.DistanceOfMinVolume != -1f ? soundEmitter.DistanceOfMinVolume : DefaultDistanceOfMinVolume;
        if (realDistance < distanceOfMaxVolume)
          soundEmitter.AudioEmitter.Position = ActiveAudioListener.Position;
        else if (realDistance > distanceOfMinVolume)
          soundEmitter.AudioEmitter.Position = new Vector3(float.MinValue, float.MinValue, float.MinValue);
        else {
          setToVirtualPosition(soundEmitter, realDistance, distanceOfMaxVolume, distanceOfMinVolume);
        }
      }
    }

    private void setToVirtualPosition(SoundEmitterComponent soundEmitter, float realDistance, float distanceOfMaxVolume, float distanceOfMinVolume) {
      // counting distance between 0 and DistanceScaleMultiplier that will be used to create virtual position of emitter
      var virtualDistance = (realDistance - distanceOfMaxVolume) * DistanceScaleMultiplier / (distanceOfMinVolume - distanceOfMaxVolume);
      var direction = VectorUtils.GetDirectionVector(VectorUtils.Vector3ToVector2(ActiveAudioListener.Position), soundEmitter.Entity.Position);
      Vector2 virtual2DPosition = VectorUtils.Vector3ToVector2(ActiveAudioListener.Position) + direction * virtualDistance;
      soundEmitter.AudioEmitter.Position = new Vector3(virtual2DPosition, soundEmitter.PositionZ);
    }

    private void updateListeners() {
      foreach (var soundListener in ECS.GetAllComponents<SoundListenerComponent>()) {
        soundListener.AudioListener.Position = new Vector3(soundListener.Entity.Position, soundListener.PositionZ);
      }
    }

    public override string ToString() {
      return "%{LightGoldenrodYellow}% SoundSystem: \n"
        + "%{PaleGoldenrod}% DefaultDistanceOfMaxVolume: %{White}% " + DefaultDistanceOfMaxVolume + "\n"
        + "%{PaleGoldenrod}% DefaultDistanceOfMinVolume: %{White}% " + DefaultDistanceOfMinVolume + "\n"
        + "%{PaleGoldenrod}% DistanceScaleMultiplier: %{White}% " + DistanceScaleMultiplier;
    }
  }
}
