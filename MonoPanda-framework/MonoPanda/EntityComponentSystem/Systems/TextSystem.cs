﻿using MonoPanda.Components;
using MonoPanda.ECS;
using MonoPanda.ECS.Components.DrawingComponent;
using System.Collections.Generic;

namespace MonoPanda.Systems {
  public class TextSystem : EntitySystem {

    private List<TextComponent> textComponents;

    public TextSystem(string id, int updateInterval = -1) : base(id, updateInterval) {
      textComponents = new List<TextComponent>();
    }

    public override void Update() {
      textComponents.Clear();
      foreach (TextComponent textComponent in ECS.GetAllComponents<TextComponent>()) {
        if (textComponent.DrawComponentNotCreated.Check())
          textComponent.Entity.AddComponent(createDrawComponent(textComponent.Depth));
      }
    }

    private DrawComponent createDrawComponent(float depth) {
      DrawComponent component = new DrawComponent();
      component.DrawTarget = new TextSprite();
      component.Depth = depth;
      return component;
    }

    public override string ToString() {
      return "%{Snow}% TextSystem";
    }
  }
}
