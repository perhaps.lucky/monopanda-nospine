﻿using Microsoft.Xna.Framework;
using MonoPanda.Components;

namespace MonoPanda.ECS.Systems.Camera {
  public class FollowedEntityObject : FollowedObject {

    private Entity entity;
    private Vector2 size;

    public FollowedEntityObject(Entity entity) {
      this.entity = entity;
      this.size = Optional<Vector2>.Of(() => entity.GetComponent<DrawComponent>().Size).OrElse(Vector2.Zero);
    }
    
    public override Vector2 GetPosition() {
      return entity.Position;
    }

    public override Vector2 GetSize() {
      return size;
    }

    public override Vector2 GetVelocity() {
      return entity.GetComponentOptional<MoveComponent>()
        .IfPresentGet(moveComponent => moveComponent.Velocity)
        .OrElse(Vector2.Zero);
    }
  }
}
