﻿using Microsoft.Xna.Framework;
using MonoPanda.CollisionComponentDetails;
using MonoPanda.Components;
using MonoPanda.ECS;
using MonoPanda.ECS.Components.CollisionComponentDetails;

namespace MonoPanda.Systems {
  public class MoveSolidCollisionSystem : EntitySystem {

    public bool PlatformerTopCollision { get; set; }

    public MoveSolidCollisionSystem(string id, int updateInterval = -1) : base(id, updateInterval) {
    }

    public override void Update() {
      foreach (var moveComponent in ECS.GetAllComponents<MoveComponent>()) {
        foreach (var solidComponent in ECS.GetAllComponents<SolidComponent>()) {
          if (moveComponent.Entity == solidComponent.Entity)
            continue;

          updateIfCollides(moveComponent, solidComponent);
        }
      }
    }

    private void updateIfCollides(MoveComponent moveComponent, SolidComponent solidComponent) {
      var collisionComponent = moveComponent.Entity.GetComponent<CollisionComponent>();
      if (collisionComponent != null) {
        var collision = collisionComponent.GetCollisionDetails(solidComponent.Entity);
        if (collision.IsColliding) {
          resolveCollision(moveComponent, solidComponent, collision);
        }
      }
    }

    private void resolveCollision(MoveComponent moveComponent, SolidComponent solidComponent, CollisionDetails collision) {
      var moveCollisionComponent = moveComponent.Entity.GetComponent<CollisionComponent>();
      var solidCollisionComponent = solidComponent.Entity.GetComponent<CollisionComponent>();

      switch (collision.Direction) {
        case CollisionDirection.Top:
          resolveTop(collision, moveCollisionComponent, solidCollisionComponent);
          break;
        case CollisionDirection.Bottom:
          resolveBottom(collision, moveCollisionComponent, solidCollisionComponent);
          break;
        case CollisionDirection.Right:
          resolveRight(collision, moveCollisionComponent, solidCollisionComponent);
          break;
        case CollisionDirection.Left:
          resolveLeft(collision, moveCollisionComponent, solidCollisionComponent);
          break;
      }
    }

    private void resolveTop(CollisionDetails collision, CollisionComponent moveCollision, CollisionComponent solidCollision) {
      var yDistance = collision.ThisCollisionRectangle.Bottom - solidCollision.CollisionMovementParams.CurrentCollisionRectangle.Top;

      moveCollision.Entity.Position = new Vector2(moveCollision.Entity.Position.X, collision.ThisCollisionRectangle.Center.Y - moveCollision.OffsetRectangle.Y - yDistance - (PlatformerTopCollision ? 0.00001f : 0.1f)); 
      // I lowered last bit a lot for top because there was a problem with platform mimicking where collision wasn't happening on every update
      // On the other hand it causes issues where big objects are going through small ones from this side

      var moveComponent = moveCollision.Entity.GetComponent<MoveComponent>();
      var solidMoveComponent = solidCollision.Entity.GetComponent<MoveComponent>();
      if (solidMoveComponent != null) {
        if (PlatformerTopCollision) {
          moveComponent.Mimicking = solidMoveComponent;
        }
        moveComponent.Velocity = new Vector2(moveComponent.Velocity.X, solidMoveComponent.Velocity.Y);
      }
    }

    private void resolveBottom(CollisionDetails collision, CollisionComponent moveCollision, CollisionComponent solidCollision) {
      var yDistance = solidCollision.CollisionMovementParams.CurrentCollisionRectangle.Bottom - collision.ThisCollisionRectangle.Top;

      moveCollision.Entity.Position = new Vector2(moveCollision.Entity.Position.X, collision.ThisCollisionRectangle.Center.Y - moveCollision.OffsetRectangle.Y + yDistance + 0.1f);

      var moveComponent = moveCollision.Entity.GetComponent<MoveComponent>();
      var solidMoveComponent = solidCollision.Entity.GetComponent<MoveComponent>();
      if(solidMoveComponent != null)
        moveComponent.Velocity = new Vector2(moveComponent.Velocity.X, solidMoveComponent.Velocity.Y);
    }

    private void resolveRight(CollisionDetails collision, CollisionComponent moveCollision, CollisionComponent solidCollision) {
      var xDistance = solidCollision.CollisionMovementParams.CurrentCollisionRectangle.Right - collision.ThisCollisionRectangle.Left;

      moveCollision.Entity.Position = new Vector2(collision.ThisCollisionRectangle.Center.X - moveCollision.OffsetRectangle.X + xDistance + 0.1f, moveCollision.Entity.Position.Y);

      var moveComponent = moveCollision.Entity.GetComponent<MoveComponent>();
      var solidMoveComponent = solidCollision.Entity.GetComponent<MoveComponent>();
      if(solidMoveComponent != null)
        moveComponent.Velocity = new Vector2(solidMoveComponent.Velocity.X, moveComponent.Velocity.Y);
    }

    private void resolveLeft(CollisionDetails collision, CollisionComponent moveCollision, CollisionComponent solidCollision) {
      var xDistance = collision.ThisCollisionRectangle.Right - solidCollision.CollisionMovementParams.CurrentCollisionRectangle.Left;

      moveCollision.Entity.Position = new Vector2(collision.ThisCollisionRectangle.Center.X - moveCollision.OffsetRectangle.X - xDistance - 0.1f, moveCollision.Entity.Position.Y);

      var moveComponent = moveCollision.Entity.GetComponent<MoveComponent>();
      var solidMoveComponent = solidCollision.Entity.GetComponent<MoveComponent>();
      if(solidMoveComponent != null)
        moveComponent.Velocity = new Vector2(solidMoveComponent.Velocity.X, moveComponent.Velocity.Y);
    }

    public override string ToString() {
      return "%{Lime}% MoveSolidCollisionSystem: \n"
        + "%{PaleGoldenrod}% PlatformerTopCollision: %{White}% " + PlatformerTopCollision;
    }
  }
}
