﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoPanda.ECS.Components.CollisionComponentDetails;
using System;

namespace MonoPanda.ECS.Tools {
  public class DrawRectangle : IDisposable {
    public CollisionRectangle Rectangle { get; set; }
    public Color Color { get; set; }
    public float Depth { get; set; }

    private Texture2D texture;

    public DrawRectangle(CollisionRectangle rectangle, Color color, float depth) {
      Rectangle = rectangle;
      Color = color;
      Depth = depth;
    }

    public DrawRectangle(Rectangle rectangle, Color color, float depth) : this(CollisionRectangle.FromRectangle(rectangle), color, depth) { }

    public void Draw(SpriteBatch spriteBatch) {
      texture = new Texture2D(GameMain.GraphicsDeviceManager.GraphicsDevice, (int)Rectangle.Width, (int)Rectangle.Height);
      Color[] data = new Color[(int)Rectangle.Width * (int)Rectangle.Height];
      for (int i = 0; i < data.Length; i++) data[i] = Color;
      texture.SetData(data);
#pragma warning disable CS0618 // Type or member is obsolete
      spriteBatch.Draw(
        texture,
        position: new Vector2(Rectangle.X, Rectangle.Y),
        color: Color.White,
        rotation: 0,
        scale: new Vector2(1f),
        layerDepth: Depth, sourceRectangle: null, origin: Vector2.Zero, effects: SpriteEffects.None);
#pragma warning restore CS0618 // Type or member is obsolete
    }

    public void Dispose() {
      if (texture != null)
        texture.Dispose();
    }
  }
}
