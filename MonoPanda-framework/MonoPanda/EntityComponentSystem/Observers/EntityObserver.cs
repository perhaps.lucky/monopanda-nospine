﻿using System;

namespace MonoPanda.ECS {
  public class EntityObserver {
    private Action updateAction;

    public EntityObserver(Action action) {
      updateAction = action;
    }

    public void Inform() {
      updateAction.Invoke();
    }
  }
}
