﻿using MonoPanda.ECS;

namespace MonoPanda.Components {
  public class GravityComponent : EntityComponent {
    public float GravityMultiplier { get; set; } = 1f;

    public override string ToString() {
      return "%{GhostWhite}% GravityComponent: \n"
        + "%{PaleGoldenrod}% GravityMultiplier: %{White}% " + GravityMultiplier;
    }
  }
}
