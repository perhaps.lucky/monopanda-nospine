﻿using MonoPanda.ECS;

namespace MonoPanda.Components {
  public class TimeComponent : EntityComponent {
    public float TimeMultiplier { get; set; } = 1.0f;
  }
}
