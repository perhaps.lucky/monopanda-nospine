﻿namespace MonoPanda.ECS.Components.LightComponent {
  public enum LightType {
    PointLight, Spotlight, TexturedLight
  }
}
