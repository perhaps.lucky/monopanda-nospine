﻿using System.Collections.Generic;

namespace MonoPanda.ECS.Components.BehaviourComponent {
  public class Behaviour {

    public Entity Entity { get; set; }

    public virtual void ApplyParameters(Dictionary<string, object> parameters) { }

    public virtual void Update() { }

  }
}
