﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoPanda.Content;
using MonoPanda.ECS;
using MonoPanda.ECS.Components.DrawingComponent;
using MonoPanda.ParameterConvert;
using MonoPanda.Utils;
using System.Collections.Generic;

namespace MonoPanda.Components {
  public class DrawComponent : EntityComponent {
    public string ContentId { private get; set; }
    public Vector2 Origin { get { return origin; } set { origin = value; HasSetOrigin = true; } }
    [ColorParameter]
    public Color Color { get; set; } = Color.White;
    public float Rotation { get; set; }
    public float Scale { get; set; } = 1f;
    public SpriteEffects SpriteEffect { get; set; }
    public float Depth { get; set; }
    public IDraw DrawTarget { get; set; }
    public bool IsVisible { get; set; } = true;
    [VectorParameter]
    public Vector2 Size { get; set; }

    private Vector2 origin;
    public bool HasSetOrigin { get; private set; }

    public override void Init(Dictionary<string, object> parameters) {
      ContentItemType itemType = ContentManager.getItem(ContentId).ItemType;
      switch (itemType) {
        case ContentItemType.Texture2D:
          DrawTarget = new Sprite(ContentId);
          break;
        case ContentItemType.SpriteSheet:
          DrawTarget = new SpriteSheetSprite(ContentId);
          break;
      }
    }

    public void Draw(SpriteBatch spriteBatch) {
      DrawTarget.Draw(spriteBatch, this);
    }

    public override string ToString() {
      return "%{Gold}% DrawComponent: \n"
        + "%{PaleGoldenrod}% ContentId: %{White}% " + ContentId + "\n"
        + "%{PaleGoldenrod}% Origin: %{White}% " + Origin + "\n"
        + "%{PaleGoldenrod}% Rotation: %{White}% " + Rotation + "\n"
        + "%{PaleGoldenrod}% Scale: %{White}% " + Scale + "\n"
        + "%{PaleGoldenrod}% Depth: %{White}% " + Depth + "\n"
        + "%{PaleGoldenrod}% SpriteEffects: %{White}% " + SpriteEffect.ToString() + "\n"
        + "%{PaleGoldenrod}% DrawTarget: %{White}% " + DrawTarget.ToString() + "\n"
        + "%{PaleGoldenrod}% Visible: " + (IsVisible ? "%{Lime}% Yes" : "%{OrangeRed}% No") + "\n"
        + "%{PaleGoldenrod}% Size: %{White}% " + Size;
    }
  }
}
