﻿using System;

namespace MonoPanda.ECS.Components.DrawingComponent {
  public class SpriteSheetAnimation {
    /// <summary>
    /// List of frames played in order
    /// </summary>
    public int[] Frames { get; set; }

    /// <summary>
    /// Duration of one frame in millis.
    /// </summary>
    public int FrameDuration { get; set; }

    /// <summary>
    /// If true, animation will start again from the first frame after reaching the end.
    /// </summary>
    public bool IsLooping { get; set; }

    /// <summary>
    /// If true, frame list will get played in reversed order after reaching the end.
    /// <b>Makes animation looped regardless of IsLooping flag.</b>
    /// </summary>
    public bool IsReversing { get; set; }

    /// <summary>
    /// Indicates if animation is currently reversed. Used inside component on restart of animation.
    /// </summary>
    public bool isReversed { get; set; }
    
  }
}
