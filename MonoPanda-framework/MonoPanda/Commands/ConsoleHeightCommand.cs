﻿using MonoPanda.Console;
using System.Collections.Generic;

namespace MonoPanda.Commands {
  public class ConsoleHeightCommand : Command {

    public ConsoleHeightCommand() {
      HelpCategoryName = HelpCategories.Console;
      ShortHelpDescription = "Changes height of console window";
      ExtendedHelp = "consoleheight %{Plum}% [height] \n\n"
        + "%{Plum}% height %{White}% - new height of console, numeric integral value";
    }

    public override string Execute(List<string> parameters) {
      int height = int.Parse(parameters[0]);
      ConsoleSystem.SetHeight(height);
      return "Height set to " + height;
    }
  }
}
