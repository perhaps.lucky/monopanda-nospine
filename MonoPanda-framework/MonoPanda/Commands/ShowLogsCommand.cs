﻿using MonoPanda.Console;
using System.Collections.Generic;

namespace MonoPanda.Commands {
  public class ShowLogsCommand : Command {
    public ShowLogsCommand() {
      HelpCategoryName = HelpCategories.Console;
      ShortHelpDescription = "Write logs in this console";
      ExtendedHelp = "showlogs %{LightPink}% ([on/off])\n\n"
                   + "%{LightPink}% on/off %{White}% - if not present or wrong value, mode will be switched";
    }

    public override string Execute(List<string> parameters) {
      if (parameters.Count <= 0 || !handleParam(parameters[0])) {
        ConsoleSystem.SetWriteLogs(!ConsoleSystem.GetWriteLogs());
      }

      return "Show logs " + (ConsoleSystem.GetWriteLogs() ? " %{Lime}% on" : " %{Red}% off");
    }

    // Returns true if parameter value is valid
    private bool handleParam(string value) {
      if (value.ToLower().Equals("on")) {
        ConsoleSystem.SetWriteLogs(true);
        return true;
      } else if (value.ToLower().Equals("off")) {
        ConsoleSystem.SetWriteLogs(false);
        return true;
      }
      return false;
    }
  }
}
