﻿using System.Collections.Generic;

namespace MonoPanda.Commands {
  public class PrintCommand : Command {

    public PrintCommand() {
      ShowInHelp = false;
    }


    public override string Execute(List<string> parameters) {
      var result = "";
      foreach (string parameter in parameters) {
        result += parameter + "\n";
      }
      return result.Substring(0, result.Length - 1); // remove new line symbol
    }
  }
}
