﻿using MonoPanda.ECS;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace MonoPanda.Commands {
  public class SearchEntityCommand : Command {

    public SearchEntityCommand() {
      HelpCategoryName = HelpCategories.World;
      ShortHelpDescription = "Search entity by Id";
      ExtendedHelp = "searchentity %{Plum}% [search_query] %{PaleGreen}% ([mode]) \n\n"
                    + "%{Plum}% search_query %{White}% - query for the search \n"
                    + "%{PaleGreen}% mode %{White}% - mode of search, available options: \n"
                    + "    %{Orange}% C %{White}% - contains query [default] \n"
                    + "    %{Orange}% R %{White}% - matches query regex \n"
                    + "    %{Orange}% S %{White}% - starts with query \n"
                    + "    %{Orange}% E %{White}% - ends with query\n";
    }

    public override string Execute(List<string> parameters) {
      string query = parameters[0];
      var mode = parameters.Count > 1 ? parameters[1] : "C";
      
      string result = "%{Yellow}% Found entities:";
      foreach (Entity entity in ECS.GetAllEntities()) {
        if (checkEntity(entity.Id, query, mode))
          result += "\n" + entity.Id;
      }
      return result;
    }

    private bool checkEntity(string id, string pattern, string mode) {
      switch (mode.ToUpper()) {
        case "C":
          return id.Contains(pattern);
        case "R":
          return Regex.IsMatch(id, pattern);
        case "S":
          return id.StartsWith(pattern);
        case "E":
          return id.EndsWith(pattern);
        default:
          return false; // wrong mode
      }
    }
  }
}
