﻿using MonoPanda.Commands.Macros;
using System.Collections.Generic;

namespace MonoPanda.Commands {
  public class ListMacrosCommand : Command {

    public ListMacrosCommand() {
      HelpCategoryName = HelpCategories.Macro;
      ShortHelpDescription = "Prints list of user defined macros";
      ExtendedHelp = "listmacros \n\n Returns list of macros defined by %{Cyan}% addmacro %{White}% command.";
    }

    public override string Execute(List<string> parameters) {
      string result = "";
      foreach (KeyValuePair<string, string> keyValuePair in MacroManager.GetMacros()) {
        result += "%{Yellow}% " + keyValuePair.Key + " %{White}% - %{LightGray}% " + keyValuePair.Value + "\n";
      }
      if (result.Length > 0)
        return result.Substring(0, result.Length - 1);
      else
        return "No macros found!";
    }
  }
}
