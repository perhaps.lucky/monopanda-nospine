﻿namespace MonoPanda.Commands {
  public class HelpCategories {
    public const string Window = "Window settings";
    public const string World = "World";
    public const string Macro = "User macros";
    public const string Console = "Console";
    public const string Game = "Game";
  }
}
