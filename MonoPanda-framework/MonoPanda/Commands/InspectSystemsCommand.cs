﻿using MonoPanda.ECS;

using System.Collections.Generic;

namespace MonoPanda.Commands {
  public class InspectSystemsCommand : Command {

    public InspectSystemsCommand() {
      HelpCategoryName = HelpCategories.World;
      ShortHelpDescription = "Prints informations about systems";
      ExtendedHelp = "inspectsystems \n\nPrints informations about systems";
    }

    public override string Execute(List<string> parameters) {
      string value = "";
      foreach (EntitySystem system in ECS.GetAllSystems()) {
        value += system.ToString() + '\n';
      }
      return value;
    }
  }
}
