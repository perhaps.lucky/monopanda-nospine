﻿using System.Collections.Generic;

namespace MonoPanda.SpriteSheets {
  public class SpriteSheetDescription {
    public List<SpriteSheetFrame> frames { get; set; }
  }
}
