﻿namespace MonoPanda.Logger {
  public class LevelSettings {
    public LogCategory Category { get; set; }
    public LogLevel Level { get; set; }
  }
}
