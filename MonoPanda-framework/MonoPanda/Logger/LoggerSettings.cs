﻿using System.Collections.Generic;

namespace MonoPanda.Logger {
  public class LoggerSettings {
    public bool FileOutputEnabled { get; set; }
    public string FileOutputDirectory { get; set; }
    public int FileSaveInterval { get; set; }
    public int MaxLogFiles { get; set; }
    public bool Timestamps { get; set; }
    public string TimestampsFormat { get; set; }
    public List<LevelSettings> LevelSettings { get; set; }
  }
}
