﻿using Microsoft.Xna.Framework.Input;
using MonoPanda.Timers;

namespace MonoPanda.Input {
  public class KeyInputData {

    public string Id { get; set; }
    public Keys Key { get; set; }

    /// <summary>
    /// Returns true on the update when button got pressed down.
    /// </summary>
    public bool IsPressed { get { return isPressed; } }
    /// <summary>
    /// Returns true each update while button is down.
    /// </summary>
    public bool IsHeld { get { return isHeld; } }
    /// <summary>
    /// Returns true on update when button got released.
    /// </summary>
    public bool IsReleased { get { return isReleased; } }

    // Fields are accessible from InputManager private class that extends KeyInputData
    protected bool isPressed;
    protected bool isHeld;
    protected bool isReleased;
    protected bool wasReleased;
    protected bool wasPressed;

    protected RepeatingTimer cdTimer;

    /// <summary>
    /// Periodically returns true while button is held. <br/>
    /// <b>Note:</b> This method can be used only in single place per update. For multiple events bound to single button holding, use Held with external timer instead.
    /// </summary>
    /// <param name="delay">time between trues in milis</param>
    /// <returns></returns>
    public bool IsHeldDelay(int delay) {
      if (isHeld) {
        if (cdTimer == null) {
          cdTimer = new RepeatingTimer(delay);
          cdTimer.Initialize();
        }

        return cdTimer.Check();
      } else if (cdTimer != null) {
        cdTimer.Dispose();
        cdTimer = null;
      }
      return false;
    }

  }
}